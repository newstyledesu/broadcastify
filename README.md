# Broadcastify Dark

## About
A dark theme for broadcastify.com with a compact navigation. Some navigation elements that used images were replaced with plain text.

## Links
Userstyles.org page: https://userstyles.org/styles/149251/broadcastify-dark

## Screenshot
![theme screenshot](broadcastify.png)

## Installation

### Step 1:
Install the Stylish or Stylus extension for Firefox or Chrome. Then, either:

### Step 2:
Install from userstyles (https://userstyles.org/styles/149251/broadcastify-dark)  
OR  
Import the *.css file into Stylish or Stylus manually